#!/bin/bash

# path of nextcloud instance
NC_Path="/var/www/cloud"
NC_UPD_Path="${NC_Path}/updater"

pushd ${NC_Path}
sudo -u www-data php occ maintenance:mode --on
popd

pushd ${NC_UPD_Path}
sudo -u www-data php updater.phar -n
popd

pushd ${NC_Path}
sudo -u www-data php occ maintenance:mode --off
popd
