#!/bin/bash
# this should be placed on the server

# Config
SNAP_DATE=$(date "+%Y-%m-%d_%H-%M")
BACKUP_ROOT_DIR="/folder"
BACKUP_DIR="/${BACKUP_ROOT_DIR}/${SNAP_DATE}"

# DB config
DB_USR=""
DB_PW=""
DB_DB=""

########## No Change below this line ############


echo "###### db backup started at: $(date) ######"
mkdir ${BACKUP_DIR}
# backup the db
mysqldump -q -u ${DB_USR} -p'${DB_PW}' -h 127.0.0.1 --single-transaction --no-tablespaces --quick --lock-tables=false ${DB_DB} > ${BACKUP_DIR}/mysql-${DB_DB}-${SNAP_DATE}.sql

tar -zcvf ${BACKUP_DIR}/mysql-${DB_DB}-${SNAP_DATE}.tar.gz ${BACKUP_DIR}/mysql-${DB_DB}-${SNAP_DATE}.sql
rm -rf ${BACKUP_DIR}/mysql-${DB_DB}-${SNAP_DATE}.sql

echo "###### db Backup ended at: $(date) ######"

echo "###### remove old backups older than 30 days ######"

find ${BACKUP_ROOT_DIR}/* -mtime +29 -exec rm {} \;


echo "###### did remove old backups older than 30 days ######"