#!/bin/bash
# this should be placed on the machine you want to have the backup

# Config
SNAP_DATE=$(date "+%F-%H_%M")
BACKUP_ROOT_DIR=""
BACKUP_DATA_DIR="${BACKUP_ROOT_DIR}/data/${SNAP_DATE}"
BACKUP_DB_DIR="${BACKUP_ROOT_DIR}/db/"
BACKUP_SOURCE="usr@srvr:/"
BACKUP_SOURCE_DB="usr@srvr:/"

#
# Steps to do:
# copy lokal files via cp -lr
# structure:
# ${BACKUP_ROOT_DIR}/
#
cp -rl ${BACKUP_ROOT_DIR}/data/current/ ${BACKUP_DATA_DIR}
rsync -aPE ${BACKUP_SOURCE} ${BACKUP_DATA_DIR}
rsync -aPE ${BACKUP_SOURCE_DB} ${BACKUP_DB_DIR}
unlink ${BACKUP_ROOT_DIR}/data/current
ln -s ${BACKUP_DATA_DIR} ${BACKUP_ROOT_DIR}/data/current